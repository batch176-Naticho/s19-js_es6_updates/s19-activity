
const cube = 7**3
const getCube = `The cube of 7 is ${cube}.`

console.log(getCube)

const address = [`777`, `P. Tirad St.`, `Sta. Ana`, `Manila`]
const [Brgy, Street, Municipalty, City] = address
console.log(`I live at ${Brgy}, ${Street}, ${Municipalty}, ${City}` )

const animal = {
	name: `Zekki`,
	age: `2`,
	breed: `persian`,
	weight: `4.75 kg`
}
function catDetail({name, age, breed, weight}){
console.log(`${name} is my cat. He is ${age} years old, his breed is ${breed} and he is ${weight}. `)
}
catDetail(animal)

const array = [7, 77, 777, 7777, 77777]
array.forEach((number) => console.log(number))



class Dog{
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}
const myDog = new Dog (`Hunter`, 4, `Jack Russell`)
console.log(myDog)